// alert('Hello')

// querySelector() is a method that can be used to select a specific element from our document
console.log(document.querySelector('#txt-first-name'));
// document refers to the whole page
console.log(document);


//     Alternative methods that we can use aside from querySelector in retrieving elements:

//     Syntax:
//         document.getElementById()
//         document.getElementByClassName()
//         document.getElementByTagName()
// 

// DOM Manipulation
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name')
const spanFirstName = document.querySelector('#span-first-name');
const spanLastName = document.querySelector('#span-last-name');
console.log(txtFirstName);
// console.log(txtLastName);
console.log(spanFirstName);

// 
//     Event:
//         click, hover, keypress, keyup and many others

//     Event listeners:
//         Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task.

//     Syntax:
//         selectedElement.addEventListener('event', function);
// 

txtFirstName.addEventListener('keyup', (event) => {
    spanFirstName.innerHTML = txtFirstName.value
});

txtLastName.addEventListener('keyup', (event) => {
    spanLastName.innerHTML = txtLastName.value
});



// 
// alternative way

// txtFirstName.addEventListener('keyup', printFirstName);

// function printFirstName (event) {
//     spanFullName.innerHTML = txtFirstName.value
// }


// txtLastName.addEventListener('keyup', printLastName);

// function printLastName (event) {
//     spanFullName.innerHTML = txtLastName.value
// }


// 
//     innerHTML - is a property of an element which considers all the children of the selected element as a string.

//     .value of the input text field.

txtFirstName.addEventListener('keyup', (event) => {
    console.log(event);
    console.log(event.target);
    console.log(event.target.value);
});

const labelFirstName = document.querySelector('#label-txt-name');

labelFirstName.addEventListener('click', (e) => {
    console.log(e);
    alert('You clicked the First Name Label.');
})